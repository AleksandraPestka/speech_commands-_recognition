# Speech Commands Recognition with CNN

The aim of this project is to build a basic speech recognition network using Python Deep Learning library - **Keras** that recognizes ten different commands, namely: "yes", "no", "up", "down", "left", "right", "on", "off", "stop", or "go". 

## Project description 

### Dataset 
The original dataset is credited to Google (**Google’s Speech Commands Dataset**).
<br> It consists about 65,000 one-second long utterances of 30 short words, by thousands of different people. <br> Every record is saved in **.wav** format and sample rate equals to **16kHz**. <br><br>
> In order to run the model download [Google's Speech Commands Dataset](http://download.tensorflow.org/data/speech_commands_v0.01.tar.gz) (1.4GB) and save in the directory *Speech_Commands_Recognition/data/raw/* <br>according to provided below tree structure. 

### From raw amplitude data to spectrograms
Instead of temporal domain we will use frequency domain. <br>
In order to convert raw data to spectrogram use **STFT** (Short-Time Fourier Transform). <br> Spectrograms can be used as a way of visualizing the change of a nonstationary signal’s frequency content over time. <br> Logarithm of spectrogram values will make our plot much more clear. It is strictly connected to the way people hear.<br> In order to assure that there are no 0 values as input to logarithm 
thus we add epsilon non-zero values in the preprocessing step. 

Below images are included in folder */plots/*. <br><br>
<img src="https://gitlab.com/AleksandraPestka/speech_commands-_recognition/raw/master/plots/raw_signals.jpeg" width="500" height="1000">
<img src="https://gitlab.com/AleksandraPestka/speech_commands-_recognition/raw/master/plots/spectrograms.jpeg" width="500" height="1000"> 

### Network architecture 
In order to handle images from spectrograms as an input to the network, we will 
use 3 blocks of **Convolutional Neural Network**. <br>

Each of them consists:
- **Conv2D layer** (with activation function: **'relu'**)  <br>filters input signal and extracts some additional image features
- **MaxPooling2D layer** <br> performs a downsampling operation reducing the size of an input with **max()** function
- **BatchNormalization layer** <br> 
increases stability
- **Dropout layer** <br> 
one of the method to avoid overfitting

Moreover, 2 Fully-connected layer at the end. <br>The last one with **softmax** 
activation function to return probability for each class. <br> 
Class with the highest probability is considered to be predicted label for provided recording. 

### Project structure 
```
Speech_Commands_Recognition/
|-- data
|   |-- predict
|   `-- raw
|       `-- speech_commands
|           |-- _background_noise_
|           |-- bed
|           |-- bird
|           |-- cat
|           |-- dog
|           |-- down
|           |-- eight
|           |-- five
|           |-- four
|           |-- go
|           |-- happy
|           |-- house
|           |-- left
|           |-- marvin
|           |-- nine
|           |-- no
|           |-- off
|           |-- on
|           |-- one
|           |-- right
|           |-- seven
|           |-- sheila
|           |-- six
|           |-- stop
|           |-- three
|           |-- tree
|           |-- two
|           |-- up
|           |-- wow
|           |-- yes
|           `-- zero
|-- logs
|-- model/
|   -- labels.pkl
|   `-- model.pkl
|-- plots
|   -- ffts.jpeg
|   -- raw_signals.jpeg
|   `-- spectrograms.jpeg
`-- scripts
|   -- eda.py
|   -- main.py
|   -- model.py
|   -- predict.py
|   `-- preprocessing.py    
```
### Scripts brief description
- eda.py <br>
Exploratory Data Analysis - plot raw signals, FFT plots and spectrograms <br>
and save in the directory *Speech_Commands_Recognition/plots/* .

- preprocessing.py <br>
Create an object called **DataGenerator()** with methods responsible for <br>
reading .wav files, converting to spectrograms, <br>
splitting data into train/validation/test sets.

- model.py <br>
CNN architecture.

- main.py <br>
Train and evaluate the model. 

- predict.py <br>
Test your own data saved in the directory *Speech_Commands_Recognition/data/predict* .

### Results 
Model accuracy increases up to ~ 97% on training set and ~ 94% on validation set. On test set it reaches ~ 93%. <br> One can track model performance using **TensorBoard** and */logs/* folder according to below command:

```
$ tensorboard --logdir logs --port 6006
```
![](/model/TensorBoard_screenshot.png)


## Technologies 
Project is created with:
- Python 3.5.2. ( Keras 2.2.4)
- TensorBoard

## Author
Aleksandra Pestka 

## Sources 
https://www.kaggle.com/c/tensorflow-speech-recognition-challenge <br>
https://ai.googleblog.com/2017/08/launching-speech-commands-dataset.html <br>
http://dkopczyk.quantee.co.uk/speech-nn/