# create CNN model
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D, BatchNormalization 

def create_model(input_shape, no_classes):
    model = Sequential()

    model.add(Conv2D(filters=32, kernel_size=(3,3), activation="relu", \
                    padding="same", strides=1, input_shape=input_shape))
    model.add(MaxPooling2D(pool_size=(3,3), strides=(2,2), padding="same"))
    model.add(BatchNormalization())
    model.add(Dropout(rate=0.2))

    model.add(Conv2D(filters=32, kernel_size=(3,3), activation="relu", \
                    padding="same", strides=1))
    model.add(MaxPooling2D(pool_size=(3,3), strides=(2,2), padding="same"))
    model.add(BatchNormalization())
    model.add(Dropout(rate=0.2))

    model.add(Conv2D(filters=32, kernel_size=(3,3), activation="relu", \
                    padding="same", strides=1))
    model.add(MaxPooling2D(pool_size=(3,3), strides=(2,2), padding="same"))
    model.add(BatchNormalization())
    model.add(Dropout(rate=0.2))

    model.add(Flatten())
    model.add(Dense(units=64, activation="relu"))
    model.add(BatchNormalization())
    model.add(Dropout(rate=0.5))

    model.add(Dense(units=no_classes, activation="softmax"))

    return model
