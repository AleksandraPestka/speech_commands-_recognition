import numpy as np
import pandas as pd
import os
import glob
from scipy.io import wavfile
from scipy.signal import stft
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical

class DataGenerator():
    def __init__(self, labels_list, sample_rate=16000):
        self.labels_list = labels_list 
        self.sample_rate = sample_rate

    def text_to_num(self, text):
        return self.labels_list.index(text)

    def num_to_text(self, label):
        return self.labels_list[label]

    def load_data(self, data_location):

        wav_files = glob.glob(os.path.join(data_location, "*/*.wav"))
        wav_files = [x.split(sep='/')[-2] + '/' + x.split(sep='/')[-1] for x in wav_files]

        data_array = []
        for filename in wav_files:
            label_txt, name = filename.split('/')  
            if label_txt in self.labels_list:
                label_num = self.text_to_num(label_txt) 
                file_path = os.path.join(data_location, filename)

                record = (label_txt, label_num, name, file_path)
                data_array.append(record)

        df = pd.DataFrame(data_array, columns=['label', 'label_id', 'user_id', 'file_path'])

        self.df = df

        return self.df

    def apply_train_val_test_split(self, test_size=0.2, val_size=0.2, random_state=42):
        self.train, self.test = train_test_split(self.df, 
                                                 test_size=test_size,
                                                 shuffle=True,
                                                 random_state=random_state)
        self.train, self.val = train_test_split(self.train,
                                                test_size=val_size,
                                                shuffle=True,
                                                random_state=random_state)
        return self.train, self.val, self.test

    def read_wavfile(self, filename):
        rate, data = wavfile.read(filename)
        return data

    def wavfile_processing(self, filename, threshold_freq=5500, epsilon=1e-10):
        signal = self.read_wavfile(filename)
        rate = self.sample_rate

        if len(signal)>rate:
            # truncate if the audio is too long
            signal = signal[:rate]

        elif len(signal)<rate:
            # add silence if the audio is too short
            delta = rate -len(signal)
            silence = np.random.randint(-100,100,rate).astype(np.float32)
            split_int = np.random.randint(0, delta)
            silence_begin = silence[:split_int]
            silence_end = silence[split_int:delta]
            signal_with_silence = np.concatenate([silence_begin, signal, silence_end])
            signal = signal_with_silence

        # Short-time Fourier Transform
        freqs, times, spectrum = stft(signal, fs=rate, nperseg = 400, \
            noverlap = 240, nfft = 512, padded = False, boundary = None)
        
        if threshold_freq is not None:
            spectrum = spectrum[freqs<=threshold_freq, :]
            freqs = freqs[freqs<=threshold_freq]
   
        # log spectrogram 
        amp = np.log(np.abs(spectrum)+epsilon)

        return np.expand_dims(amp, axis=2) #get the 3D data
    
    def apply_processing(self):
        self.X_train, self.X_val, self.X_test = [], [], []
        self.y_train, self.y_val, self.y_test = [], [], []

        for filename, label_index  in zip(self.train["file_path"], self.train["label_id"]):
            self.X_train.append(self.wavfile_processing(filename))
            self.y_train.append(label_index)

        for filename, label_index  in zip(self.val["file_path"], self.val["label_id"]):
            self.X_val.append(self.wavfile_processing(filename))
            self.y_val.append(label_index)

        for filename, label_index  in zip(self.test["file_path"], self.test["label_id"]):
            self.X_test.append(self.wavfile_processing(filename))
            self.y_test.append(label_index)

        # one-hot encoding
        self.y_train, self.y_val, self.y_test = \
            [to_categorical(data, num_classes = len(self.labels_list)) \
            for data in [self.y_train, self.y_val, self.y_test]]
    
        return np.array(self.X_train), np.array(self.X_val), \
            np.array(self.X_test), self.y_train, self.y_val, self.y_test

