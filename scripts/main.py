# run script as: $ python3 -l <absolute path to the project>

import os
import argparse
from datetime import datetime
import pickle
from keras.callbacks import EarlyStopping, TensorBoard

from preprocessing import DataGenerator
from model import create_model

def command_line_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--location", \
        default="/home/ola/PROJECTS/Speech_Commands_Recognition", \
        help="Path to project") # use /home/ola/ instead of ~/
    args = vars(parser.parse_args())
    return args

def save_model(model_to_save, labels_to_save, model_location):
    # Save the trained model as a pickle string. 
    pickle.dump(model_to_save,\
         open(os.path.join(model_location, "model.pkl"), 'wb'))
    pickle.dump(labels_to_save,\
         open(os.path.join(model_location, "labels.pkl"), 'wb'))

if __name__=="__main__":
    SAMPLE_RATE = 16000 # constant; thus freqs are in range [0,8000]
    MAIN_LOCATION= command_line_args()["location"]
    DATA_LOCATION = os.path.join(MAIN_LOCATION, "data", "raw", "speech_commands")
    TB_LOCATION = os.path.join(MAIN_LOCATION, "logs", \
                               "Con2D_model_"+datetime.now().strftime("%H:%M:%S"))

    LABELS = "on off yes no up down right left stop go".split()
    
    ### Preparation ###

    dataObject = DataGenerator(LABELS)
    dataframe = dataObject.load_data(DATA_LOCATION)
    df_train, df_val, df_test = dataObject.apply_train_val_test_split()
    X_train, X_val, X_test, y_train, y_val, y_test = dataObject.apply_processing()

    ### Model ###

    input_size = X_train.shape[1:]
    no_classes = len(LABELS)
    epochs = 50
    tb_callback = TensorBoard(log_dir=TB_LOCATION,
                              write_grads=True,
                              write_graph=True,
                              write_images=True)

    model = create_model(input_size, no_classes)
    model.summary()

    model.compile(optimizer="Adam",
                  loss="categorical_crossentropy",
                  metrics=["acc"])

    model.fit(X_train,
              y_train,
              epochs=epochs,
              validation_data=(X_val,y_val),
              callbacks=[tb_callback])

    score = model.evaluate(X_test, y_test)
    
    print("Accuracy for test set: {:.3f}".format(score[1]))

    save_model(model, LABELS, os.path.join(MAIN_LOCATION, "model"))
