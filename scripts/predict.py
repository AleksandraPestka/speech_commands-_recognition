import pickle
import glob
import argparse
import os
import numpy as np
from scipy.io import wavfile

from preprocessing import DataGenerator

def command_line_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--location", \
        default="/home/ola/PROJECTS/Speech_Commands_Recognition", \
        help="Path to project") # use /home/ola/ instead of ~/ 
    args = vars(parser.parse_args())
    return args

def read_model(path):
    model = pickle.load(open(os.path.join(path, "model.pkl"), 'rb'))
    labels = pickle.load(open(os.path.join(path, "labels.pkl"), 'rb'))
    return model, labels

def read_wavfile(path):
    rate, data = wavfile.read(path)
    return rate, data

if __name__=="__main__":
    MAIN_LOCATION = command_line_args()["location"]
    PREDICT_LOCATION = os.path.join(MAIN_LOCATION, "data", "predict")
    PICKLE_LOCATION = os.path.join(MAIN_LOCATION, "model")
    SAMPLE_RATE = 16000 

    model, labels = read_model(PICKLE_LOCATION)
    signals_paths = glob.glob(os.path.join(PREDICT_LOCATION, "*.wav"))

    good_pred = 0

    for path in signals_paths:
        signal_label = os.path.basename(path).split("2019")[0].lower()
        print("\nLabel for file: {}".format(signal_label))
        signal_rate , signal_audio = read_wavfile(path)

        # check rate 
        if signal_rate != SAMPLE_RATE:
            raise ValueError('Signal rate should be equal to {}'.format(SAMPLE_RATE))

        dataObject = DataGenerator(labels)
        processed_data = dataObject.wavfile_processing(path)
        expand_data = np.expand_dims(processed_data, axis=0) # 4 dims

        # predictions
        probability = model.predict(expand_data)
        print("Probability for each label: \n {} \n {}".format(labels, probability))
        num_prediction = np.argmax(probability)
        text_prediction = dataObject.num_to_text(num_prediction)
        print("\nPrediction : {}".format(text_prediction))

        if text_prediction == signal_label:
            good_pred += 1

    # overall accuracy 
    acc = good_pred/len(signals_paths)
    print("\nAccuracy for the whole of provided dataset: {:.4f}".format(acc))
