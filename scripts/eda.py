# EDA (aka Exploratory Data Analysis)
# run script as: $ python3 eda.py -l <Path to project>

from scipy.io import wavfile
from scipy import signal, fftpack
import numpy as np
import matplotlib.pyplot as plt
import os
import argparse

def command_line_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--location", \
        default="/home/ola/PROJECTS/Speech_Commands_Recognition", \
        help="Path to project") # use /home/ola/ instead of ~/
    args = vars(parser.parse_args())
    return args

def get_info_wav(filename):
    global DATA_LOCATION 

    # embedding audio file to a vector
    rate, data = wavfile.read(os.path.join(DATA_LOCATION, filename))
    return rate, data 

def plot_raw_signal(filename_list):
    global DATA_LOCATION, PLOTS_LOCATION

    fig = plt.figure(figsize=(10,20))
    for idx, filename in enumerate(filename_list):
      rate, data = get_info_wav(filename)

      time_step = 1/rate
      t = np.arange(0, len(data)//rate, time_step)

      ax = fig.add_subplot(len(filename_list),1,idx+1)
      ax.plot(t, data)
      ax.set_xlabel("Time [s]")
      ax.set_ylabel("Amplitude ")
      ax.set_title("Raw wave of: " + filename)

    fig.tight_layout()
    fig.savefig(PLOTS_LOCATION+"/raw_signals.jpeg") 

def fft_compute(data, rate):
    Fs = rate
    Ts = 1/Fs

    # the FFT of the signal
    y_fft = fftpack.fft(data)
    # the power of the signal (y_fft is a complex dtype)
    power = np.abs(y_fft)
    # normalized power 
    power_norm = power/max(power)
    # The corresponding frequencies
    sample_freq = fftpack.fftfreq(Fs, d=Ts)
    # only positive freqs
    positive_mask = np.where(sample_freq > 0)
    freqs_to_plot = sample_freq[positive_mask]
    power_to_plot = power_norm[positive_mask]

    return freqs_to_plot, power_to_plot

def log_spectrogram(data, epsilon=1e-10):
    global SAMPLE_RATE

    freqs, times, spec = signal.stft(data, SAMPLE_RATE, nperseg = 400, \
        noverlap = 240, nfft = 512, padded = False, boundary = None)
    # Log spectrogram
    amp = np.log(np.abs(spec)+epsilon)
    
    return freqs, times, amp

def plot_spectrogram(filename_list):
    global DATA_LOCATION, PLOTS_LOCATION, SAMPLE_RATE

    fig_1 = plt.figure(figsize=(10,20)) #for spectrogram
    fig_2 = plt.figure(figsize=(10,20)) #for fft

    for idx, filename in enumerate(filename_list):
      _ , data = get_info_wav(filename)
      freqs, times, log_spec = log_spectrogram(data)

      # spectrograms
      ax_1 = fig_1.add_subplot(len(filename_list),1,idx+1)
      ax_1.imshow(log_spec, aspect='auto', origin='lower', 
           extent=[times.min(), times.max(), freqs.min(), freqs.max()])
      ax_1.set_xlabel("Time [s]")
      ax_1.set_ylabel("log-frequency [Hz] ")
      ax_1.set_title("Log-spectrogram of: " + filename)

      # ffts
      x_fft, y_fft = fft_compute(data, SAMPLE_RATE)
      ax_2 = fig_2.add_subplot(len(filename_list),1,idx+1)
      ax_2.plot(x_fft, y_fft)
      ax_2.set_xlabel("Freq [Hz]")
      ax_2.set_ylabel("Amplitude ")
      ax_2.set_title("Wave in frequency domain: " + filename)

    fig_1.tight_layout()
    fig_1.savefig(PLOTS_LOCATION+"/spectrograms.jpeg") 

    fig_2.tight_layout()
    fig_2.savefig(PLOTS_LOCATION+"/ffts.jpeg") 

if  __name__=="__main__":
    SAMPLE_RATE = 16000 # constant; thus freqs are in range [0,8000]
    MAIN_LOCATION= command_line_args()["location"]
    DATA_LOCATION = os.path.join(MAIN_LOCATION, "data", "raw", "speech_commands")
    PLOTS_LOCATION = os.path.join(MAIN_LOCATION, "plots")

    records_to_visual = ["go/1cec8d71_nohash_0.wav","go/1cec8d71_nohash_1.wav",\
                      "go/38d78313_nohash_0.wav", "wow/1bb6ed89_nohash_0.wav",\
                      "wow/1bb6ed89_nohash_1.wav", "wow/541120c7_nohash_0.wav"]
  
    plot_raw_signal(records_to_visual)
    plot_spectrogram(records_to_visual)

